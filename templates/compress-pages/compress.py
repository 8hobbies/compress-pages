#!/usr/bin/python3

#  Copyright (C) 2024 8 Hobbies, LLC <hong@8hobbies.com>
#
# Permission to use, copy, modify, and/or distribute this software for anypurpose with or without
# fee is hereby granted.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIESWITH REGARD TO THIS
# SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OFMERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
# AUTHOR BE LIABLE FORANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY
# DAMAGESWHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN ANACTION OF CONTRACT,
# NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OFOR IN CONNECTION WITH THE USE OR PERFORMANCE OF
# THIS SOFTWARE.

from pathlib import Path
import subprocess
import sys


if __name__ != '__main__':
    print("Can't be used as a library.", file=sys.stderr)
    sys.exit(1)

# A list of file suffices to be compressed
COMPRESSION_SUFFICES = frozenset(('.css', '.htm', '.html', '.js', '.text', '.txt', '.xml', '.svg', '.json'))

def _compress_gzip(f: Path) -> None:
    "Gzip the given file."
    gzip_file = Path(f.with_suffix(f.suffix + '.gz'))
    if gzip_file.exists():  # compressed file already exists, skip
        return

    # Use the zopfli command instead of the Python gzip module or gzip, since it
    # usually generates the highest compression ratio.
    if subprocess.run(('/usr/bin/zopfli', f)).returncode != 0:
        raise RuntimeError(f"Failed to compress {f}")

    if gzip_file.stat().st_size >= f.stat().st_size:
        # Compressed file is larger, remove it.
        gzip_file.unlink()

def _compress_brotli(f:Path) -> None:
    "Compress the given file with brotli."
    brotli_file = Path(f.with_suffix(f.suffix + '.br'))
    if brotli_file.exists():  # compressed file already exists, skip
        return

    # Use the brotli command instead of the Python package, because the Python
    # package doesn't support streaming, which may cause issues for large
    # files.
    if subprocess.run(('/usr/bin/brotli', '-k', '--best', f)).returncode != 0:
        raise RuntimeError(f"Failed to compress {f}")

    if brotli_file.stat().st_size >= f.stat().st_size:
        # Compressed file is larger, remove it.
        brotli_file.unlink()

def main() -> int:
    public_dir = Path('public')
    if not public_dir.is_dir():
        # No public dir found. Do nothing.
        return 0

    for f in public_dir.glob('**/*'):
        if not f.is_file():
            continue
        if f.suffix in ('.gz', '.br'):
            # Already a compressed file.
            continue
        if f.suffix not in COMPRESSION_SUFFICES:
            # Not a file of interest.
            continue

        _compress_gzip(f)
        _compress_brotli(f)

    return 0


sys.exit(main())
